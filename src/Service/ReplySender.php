<?php

namespace Drupal\webform_email_reply_threads\Service;

use Drupal\awareness\Database\DatabaseAwareTrait;
use Drupal\awareness\DateTime\TimeAwareTrait;
use Drupal\awareness\File\FileSystemAwareTrait;
use Drupal\awareness\File\FileUrlGeneratorAwareTrait;
use Drupal\awareness\Mail\MailPluginManagerAwareTrait;
use Drupal\awareness\Session\CurrentUserAwareTrait;
use Drupal\Core\Language\LanguageInterface;
use Drupal\file\FileInterface;
use Drupal\webform_email_reply_threads\Util\EmailAddressFormatter;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;

/**
 * Reply sending service.
 */
class ReplySender {

  use CurrentUserAwareTrait;
  use DatabaseAwareTrait;
  use FileSystemAwareTrait;
  use FileUrlGeneratorAwareTrait;
  use MailPluginManagerAwareTrait;
  use TimeAwareTrait;
  use ThreadTrackingAwareTrait;

  /**
   * Send a reply.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param string $message_key
   *   The key of the message.
   * @param int $sender_id
   *   The sender's recipient record ID.
   * @param string|null $body
   *   Reply body/message. Defaults to NULL, where no message will be sent.
   * @param \Drupal\file\FileInterface|null $attachment
   *   The attachment.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function sendReply(WebformEmailReplyThreadInterface $thread, string $message_key, int $sender_id, string|null $body = NULL, FileInterface $attachment = NULL): void {
    $recipients = $this->getThreadTracking()->getThreadRecipients($thread);

    $sender = $recipients[$sender_id];
    $from = EmailAddressFormatter::formatArray($sender);

    $params = [
      'from' => $from,
      'subject' => $thread->label(),
      'sender' => $sender,
      'thread' => $thread,
    ];

    // Ensure the attachement is permanently saved.
    if ($attachment) {
      if (!$attachment->isPermanent()) {
        $attachment->setPermanent();
        $attachment->save();
      }
    }

    if ($body !== NULL) {
      $data = [
        'uid' => $this->getCurrentUser()->id(),
        'message' => $body,
        'fid' => $attachment?->id(),
      ];
      $reply_id = $this->recordSend($thread, $sender, $data);
      $params['reply'] = $this->getThreadTracking()->getReplyById($reply_id);
    }

    // Dispatch the notification messages.
    foreach ($recipients as $recipient) {
      $is_original_sender = $recipient['id'] == $sender_id;

      // Do not send to inactive recipients.
      if (!$is_original_sender && empty($recipient['status'])) {
        continue;
      }
      $key = $message_key;

      switch ($message_key) {
        case 'new_thread':
          // Different messages for sender and recipient.
          if ($is_original_sender) {
            $key .= '_sender';
          }
          else {
            $key .= '_recipient';
          }
          break;

        case 'thread_reply':
          if ($recipient['id'] == $sender_id) {
            // Skip the sender for their reply.
            continue 2;
          }
          break;
      }

      $params['recipient'] = $recipient;
      $this->getMailPluginManager()
        ->mail('webform_email_reply_threads', $key, $recipient['mail'], LanguageInterface::LANGCODE_NOT_SPECIFIED, $params);
    }
  }

  /**
   * Function to insert the email into the database.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param array $sender
   *   Recipient data for the sender.
   * @param array $data
   *   The values to insert into the database.
   *
   * @return int
   *   The ID of the inserted record.
   *
   * @throws \Exception
   *
   * @see webform_email_reply_insert()
   */
  protected function recordSend(WebformEmailReplyThreadInterface $thread, array $sender, array $data): int {
    $reply_id = $this->getDatabase()->insert('webform_email_reply')
      ->fields([
        'eid' => NULL,
        'sid' => $thread->getSubmission()->id(),
        'webform_id' => $thread->getSubmission()->getWebform()->id(),
        'uid' => $data['uid'],
        'from_address' => EmailAddressFormatter::formatArray($sender),
        'replied' => $this->getTime()->getCurrentTime(),
        'message' => $data['message'],
        'fid' => $data['fid'],
      ])
      ->execute();
    $this->getThreadTracking()->recordThreadReply($thread, $reply_id, $sender['id']);
    return $reply_id;
  }

}
