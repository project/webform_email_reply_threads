<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Entity;

use Drupal\awareness\DateTime\TimeAwareTrait;
use Drupal\awareness\Token\TokenAwareTrait;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;

/**
 * Defines the webform email reply thread entity class.
 *
 * @ContentEntityType(
 *   id = "webform_email_reply_thread",
 *   label = @Translation("Webform Email Reply Thread"),
 *   label_collection = @Translation("Webform Email Reply Threads"),
 *   label_singular = @Translation("webform email reply thread"),
 *   label_plural = @Translation("webform email reply threads"),
 *   label_count = @PluralTranslation(
 *     singular = "@count webform email reply thread",
 *     plural = "@count webform email reply threads",
 *   ),
 *   bundle_label = @Translation("Webform Email Reply Thread type"),
 *   bundle_entity_type = "webform_email_reply_thread_type",
 *   handlers = {
 *     "list_builder" = "Drupal\webform_email_reply_threads\WebformEmailReplyThreadListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\webform_email_reply_threads\Form\WebformEmailReplyThreadForm",
 *       "edit" = "Drupal\webform_email_reply_threads\Form\WebformEmailReplyThreadForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\webform_email_reply_threads\Routing\WebformEmailReplyThreadHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "webform_email_reply_thread",
 *   admin_permission = "administer webform_email_reply_threads",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   links = {
 *     "collection" = "/admin/content/webform-email-reply-thread",
 *     "canonical" = "/webform/{webform}/reply/{webform_email_reply_thread}/{uuid}",
 *     "edit-form" = "/admin/structure/webform/manage/{webform}/submission/{webform_submission}/threads/{webform_email_reply_thread}/edit",
 *     "delete-form" = "/admin/structure/webform/manage/{webform}/submission/{webform_submission}/threads/{webform_email_reply_thread}/delete",
 *     "delete-multiple-form" = "/admin/content/webform-email-reply-thread/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.webform_email_reply_thread_type.edit_form",
 * )
 */
class WebformEmailReplyThread extends ContentEntityBase implements WebformEmailReplyThreadInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;
  use TimeAwareTrait;
  use TokenAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    if (!$this->label->value) {
      $this->label = $this->getDefaultSubject();
    }
    if (!$this->getOwnerId()) {
      // If no owner has been set explicitly, make the anonymous user the owner.
      $this->setOwnerId(0);
    }
    if ($this->getLastActivity() === NULL) {
      $this->setLastActivity($this->getTime()->getCurrentTime());
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Thread name'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the webform email reply thread was created.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the webform email reply thread was last edited.'));

    $fields['last_activity'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Last activity'))
      ->setDescription(t('The time that the webform email reply thread had activity (create/reply/etc.).'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['submission'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Submission'))
      ->setSetting('target_type', 'webform_submission')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $parameters = parent::urlRouteParameters($rel);

    $templates = $this->linkTemplates();
    if (!empty($templates[$rel])) {
      if ($submission = $this->getSubmission()) {
        if (str_contains($templates[$rel], '{webform_submission}')) {
          $parameters['webform_submission'] = $submission->id();
        }
        if (str_contains($templates[$rel], '{webform}')) {
          $parameters['webform'] = $submission->getWebform()->id();
        }
      }
    }

    // @todo add uuid if needed.
    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmission(): ?WebformSubmissionInterface {
    return $this->submission->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() {
    $tags = parent::getCacheTags();

    if ($submission = $this->getSubmission()) {
      $tags = Cache::mergeTags($tags, $submission->getCacheTags());
    }

    return $tags;
  }

  /**
   * {@inheritdoc}
   */
  public function getLastActivity(): int|null {
    return $this->last_activity->isEmpty() ? NULL : (int) $this->last_activity->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setLastActivity(int $value): static {
    $this->last_activity = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultSubject() {
    /** @var \Drupal\Core\Config\Entity\ConfigEntityInterface $type */
    $type = $this->entityTypeManager()
      ->getStorage('webform_email_reply_thread_type')
      ->load($this->bundle());
    $data = [
      'webform' => $this->getSubmission()->getWebform(),
      'webform_submission' => $this->getSubmission(),
    ];
    return $this->getToken()->replace($type->get('default_subject'), $data);
  }

}
