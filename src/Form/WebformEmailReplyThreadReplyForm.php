<?php

namespace Drupal\webform_email_reply_threads\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;

/**
 * Form for sending webform email reply from a thread.
 */
class WebformEmailReplyThreadReplyForm extends FormBase {

  use WebformEmailReplyThreadReplyFormTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_email_reply_thread_reply_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformEmailReplyThreadInterface $thread = NULL) {
    $form = $form + $this->buildReplyForm($thread);

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Send'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->sendReply($form, $form_state);
  }

}
