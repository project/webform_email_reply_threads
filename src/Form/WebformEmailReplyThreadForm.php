<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;

/**
 * Form controller for the webform email reply thread entity edit forms.
 */
class WebformEmailReplyThreadForm extends ContentEntityForm {

  use WebformEmailReplyThreadReplyFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    assert($entity instanceof WebformEmailReplyThreadInterface);

    $form['webform_submission'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Webform submission'),
    ];

    $form['webform_submission']['content'] = $this->entityTypeManager
      ->getViewBuilder('webform_submission')
      ->view($entity->getSubmission());

    // Only display the reply form when creating a new thread.
    if ($entity->isNew()) {
      $form['webform_email_reply_form'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Reply'),
      ] + $this->buildReplyForm($entity);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->toLink()->toString()];
    $logger_args = [
      '%label' => $this->entity->label(),
      'link' => $this->entity->toLink($this->t('View'))->toString(),
    ];

    switch ($result) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('New webform email reply thread %label has been created.', $message_args));
        $this->logger('webform_email_reply_threads')->notice('New webform email reply thread %label has been created.', $logger_args);
        if (!empty($form_state->getValue('webform_email_reply_form'))) {
          $form['webform_email_reply_form']['#thread'] = $this->entity;
          $this->sendReply($form['webform_email_reply_form'], $form_state);
        }
        break;

      case SAVED_UPDATED:
        $this->messenger()->addStatus($this->t('The webform email reply thread %label has been updated.', $message_args));
        $this->logger('webform_email_reply_threads')->notice('The webform email reply thread %label has been updated.', $logger_args);
        break;

      default:
        throw new \LogicException('Could not save the entity.');
    }

    $form_state->setRedirectUrl($this->entity->toUrl());

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntity() {
    parent::prepareEntity();

    if (!$this->entity->label->value) {
      assert($this->entity instanceof WebformEmailReplyThreadInterface);
      $this->entity->label = $this->entity->getDefaultSubject();
    }
  }

}
