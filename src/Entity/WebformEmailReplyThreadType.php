<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Entity;

use Drupal\awareness\Entity\EntityTypeManagerAwareTrait;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface;

/**
 * Defines the webform email reply thread type entity type.
 *
 * @ConfigEntityType(
 *   id = "webform_email_reply_thread_type",
 *   label = @Translation("Webform Email Reply Thread Type"),
 *   label_collection = @Translation("Webform Email Reply Thread Types"),
 *   label_singular = @Translation("webform email reply thread type"),
 *   label_plural = @Translation("webform email reply thread types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count webform email reply thread type",
 *     plural = "@count webform email reply thread types",
 *   ),
 *   handlers = {
 *     "list_builder" =
 *   "Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeListBuilder",
 *     "form" = {
 *       "add" =
 *   "Drupal\webform_email_reply_threads\Form\WebformEmailReplyThreadTypeForm",
 *       "edit" =
 *   "Drupal\webform_email_reply_threads\Form\WebformEmailReplyThreadTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "webform_email_reply_thread_type",
 *   admin_permission = "administer webform_email_reply_threads",
 *   links = {
 *     "collection" = "/admin/structure/webform-email-reply-thread-type",
 *     "add-form" = "/admin/structure/webform-email-reply-thread-type/add",
 *     "edit-form" = "/admin/structure/webform/webform-email-reply-thread-types/{webform_email_reply_thread_type}",
 *     "delete-form" = "/admin/structure/webform/webform-email-reply-thread-types/{webform_email_reply_thread_type}/delete",
 *     "webform-settings" = "/admin/structure/webform/manage/{webform}/settings/threads"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "label",
 *     "id",
 *     "default_subject",
 *     "messages",
 *   },
 * )
 */
class WebformEmailReplyThreadType extends ConfigEntityBase implements WebformEmailReplyThreadTypeInterface {

  use EntityTypeManagerAwareTrait;

  /**
   * The thread type ID.
   */
  protected string|null $id;

  /**
   * The human-readable name of the webform email reply type.
   *
   * @var string
   */
  protected string|null $label;

  /**
   * The default subject for the webform email reply type.
   *
   * @var string
   */
  protected string|null $default_subject;

  /**
   * Message configuration.
   */
  protected array $messages = [];

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    parent::calculateDependencies();

    // Depend on text filters.
    foreach ($this->messages as $message) {
      $this->addDependency('config', 'filter.format.' . $message['message']['format']);
    }

    return $this;
  }

}
