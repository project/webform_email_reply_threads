<?php

namespace Drupal\webform_email_reply_threads\Plugin\WebformHandler;

use Drupal\awareness\Entity\EntityTypeManagerAwareTrait;
use Drupal\awareness\Mail\MailPluginManagerAwareTrait;
use Drupal\awareness\Session\CurrentUserAwareTrait;
use Drupal\awareness\Utility\EmailValidatorAwareTrait;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\webform\Element\WebformSelectOther;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_email_reply_threads\Service\ReplySenderAwareTrait;
use Drupal\webform_email_reply_threads\Service\ThreadTracking;
use Drupal\webform_email_reply_threads\Service\ThreadTrackingAwareTrait;
use Drupal\webform_email_reply_threads\Util\EmailAddressFormatter;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Creates a thread on webform submission.
 *
 * @WebformHandler(
 *   id = "webform_email_reply_thread_create",
 *   label = @Translation("Thread create"),
 *   category = @Translation("Notification"),
 *   description = @Translation("Creates a webform email reply thread upon submission completion."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_REQUIRED,
 *   tokens = TRUE,
 * )
 */
class ThreadCreateHandler extends WebformHandlerBase {

  use CurrentUserAwareTrait;
  use EmailValidatorAwareTrait;
  use EntityTypeManagerAwareTrait;
  use MailPluginManagerAwareTrait;
  use ReplySenderAwareTrait;
  use ThreadTrackingAwareTrait;

  /**
   * Thread creation access check.
   *
   * @var \Drupal\webform_email_reply_threads\Access\WebformEmailReplyThreadTypeCreateAccess
   */
  protected $threadTypeCreateAccessCheck;

  /**
   * The webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->threadTypeCreateAccessCheck = $container->get('access_check.webform_email_reply_threads.webform_email_reply_thread_type_create');
    $instance->elementManager = $container->get('plugin.manager.webform.element');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $summary = [];

    $type = $this->getEntityTypeManager()
      ->getStorage('webform_email_reply_thread_type')
      ->load($this->configuration['type']);

    $summary['type']['#type'] = 'container';
    $summary['type']['label']['#plain_text'] = $this->t('Type:');
    $summary['type']['label']['#prefix'] = '<strong>';
    $summary['type']['label']['#suffix'] = '</strong> ';
    $summary['type']['value']['#plain_text'] = $type ? $type->label() : $this->t('Type missing');

    $summary['sender']['#type'] = 'container';
    $summary['sender']['label']['#plain_text'] = $this->t('Sender:');
    $summary['sender']['label']['#prefix'] = '<strong>';
    $summary['sender']['label']['#suffix'] = '</strong> ';
    $summary['sender']['value']['#plain_text'] = EmailAddressFormatter::formatArray($this->configuration['sender']);

    $summary['recipients']['#type'] = 'container';
    $summary['recipients']['label']['#plain_text'] = $this->t('Recipients:');
    $summary['recipients']['label']['#prefix'] = '<strong>';
    $summary['recipients']['label']['#suffix'] = '</strong> ';

    $items = array_map(function ($recipient) {
      return EmailAddressFormatter::formatArray($recipient);
    }, $this->configuration['recipients']);

    $summary['recipients']['list'] = [
      '#theme' => 'item_list',
      '#items' => $items,
    ];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'type' => 'default',
      'recipients' => [],
      'sender' => [
        'mail' => NULL,
        'name' => NULL,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $this->applyFormStateToConfiguration($form_state);

    $form['thread_type'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Thread type'),
    ];

    $types = $this->getEntityTypeManager()
      ->getStorage('webform_email_reply_thread_type')
      ->loadMultiple();
    $options = array_map(function ($type) {
      return $type->label();
    }, $types);

    $form['thread_type']['type'] = [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $this->configuration['type'],
      '#required' => TRUE,
    ];

    $form['sender'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sender'),
      '#description' => $this->t('The initial sender of the reply thread, likely the user submitting the webform.'),
    ];

    $form['sender']['name'] = $this->buildSelect($this->t('Name'), $this->t('Sender name'), '', $this->getElementOptions('name'), $this->configuration['sender']['name']);
    $form['sender']['mail'] = $this->buildSelect($this->t('Mail'), $this->t('Sender mail'), '', $this->getElementOptions('mail'), $this->configuration['sender']['mail'], TRUE);

    $form['recipients'] = $this->buildRecipientsForm($form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->applyFormStateToConfiguration($form_state);

    // If there is a name, require the mail.
    $has_recipient = FALSE;
    foreach ($this->configuration['recipients'] as $key => $recipient) {
      if (!empty($recipient['name']) && empty($recipient['mail'])) {
        $form_state->setErrorByName("recipients][$key][mail", $this->t('The recipient email address is required.'));
      }
      if (!empty($recipient['mail'])) {
        $has_recipient = TRUE;
      }
    }

    // Need at least 1 recipient mail.
    if (!$has_recipient) {
      $form_state->setErrorByName('recipients', $this->t('At least one recipient email address is required.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Remove blank recipients.
    $this->configuration['recipients'] = array_filter($this->configuration['recipients'], function ($recipient) {
      return !empty($recipient['mail']);
    });
    $this->configuration['recipients'] = array_values($this->configuration['recipients']);
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Element validation for the add recipient button.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  public function validateAddRecipient(array &$element, FormStateInterface $form_state) {
    if (!empty($form_state->getTriggeringElement()['#op']) && ($form_state->getTriggeringElement()['#op'] != $element['#op'])) {
      return;
    }

    $recipient_count = $form_state->get('recipient_count');
    $recipient_count++;
    $form_state->set('recipient_count', $recipient_count);
  }

  /**
   * AJAX callback for the add recipient button.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   The recipients section of the form.
   */
  public function ajaxAddRecipient(array &$form, FormStateInterface $form_state) {
    $parents = array_slice($form_state->getTriggeringElement()['#array_parents'], 0, -1);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * Builds the recipients section of the form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   An array for use as the recipients section of the form.
   */
  protected function buildRecipientsForm(FormStateInterface $form_state) {
    $recipients_id = Html::getUniqueId('recipients');

    $form = [
      '#id' => $recipients_id,
      '#type' => 'fieldset',
      '#title' => $this->t('Recipients'),
      '#description' => $this->t('Recipient users to add to the thread.'),
    ];

    if ($form_state->has('recipient_count')) {
      $recipient_count = $form_state->get('recipient_count');
    }
    else {
      $recipient_count = max(1, count($this->configuration['recipients']));
      $form_state->set('recipient_count', $recipient_count);
    }

    $mail_options = $this->getElementOptions('mail');
    $name_options = $this->getElementOptions('name');

    for ($key = 0; $key < $recipient_count; $key++) {
      $form[$key] = ['#type' => 'fieldset'];
      $form[$key]['name'] = $this->buildSelect($this->t('Name'), $this->t('Recipient name'), '', $name_options, $this->configuration['recipients'][$key]['name'] ?? NULL);
      $form[$key]['mail'] = $this->buildSelect($this->t('Mail'), $this->t('Recipient mail'), '', $mail_options, $this->configuration['recipients'][$key]['mail'] ?? NULL);
    }

    $ajax = [
      'callback' => [$this, 'ajaxAddRecipient'],
      'wrapper' => $recipients_id,
    ];

    $form['add_recipient'] = [
      '#type' => 'button',
      '#value' => $this->t('Add recipient'),
      '#ajax' => $ajax,
      '#weight' => count($this->configuration['recipients']) + 1000,
      '#limit_validation_errors' => [],
      '#element_validate' => [
        [$this, 'validateAddRecipient'],
      ],
      '#op' => 'add_recipient',
    ];

    return $form;
  }

  /**
   * Builds select or other element.
   *
   * @param string $title
   *   Element title, "Name" for example.
   * @param string $long_title
   *   Long element title, "Sender name" for example.
   * @param string $description
   *   Element description.
   * @param array $options
   *   Selectable options.
   * @param string $default_value
   *   Default value.
   * @param bool $required
   *   Indicates if the input ids required.
   *
   * @return array
   *   Select or other element.
   */
  protected function buildSelect(string $title, string $long_title, string $description, array $options, $default_value, bool $required = FALSE) {
    $options[WebformSelectOther::OTHER_OPTION] = $this->t('Custom @label…', ['@label' => $long_title]);
    return [
      '#type' => 'webform_select_other',
      '#title' => $title,
      '#description' => $description,
      '#options' => $options,
      '#empty_option' => (!$required) ? $this->t('- None -') : NULL,
      '#other__title' => $long_title,
      '#other__title_display' => 'invisible',
      '#other__placeholder' => $this->t('Enter @label…', ['@label' => $long_title]),
      '#other__type' => 'textfield',
      '#other__allow_tokens' => TRUE,
      '#required' => $required,
      '#default_value' => $default_value,
    ];
  }

  /**
   * Get options for the select or other elements.
   *
   * @param string $type
   *   The type of element, "mail" or "name".
   *
   * @return array
   *   The select options.
   */
  protected function getElementOptions(string $type): array {
    $options = [];

    $elements = $this->webform->getElementsInitializedAndFlattened();
    foreach ($elements as $element_key => $element) {
      $element_plugin = $this->elementManager->getElementInstance($element);
      if (!$element_plugin->isInput($element) || !isset($element['#type'])) {
        continue;
      }

      $params = ['@title' => $element['#title'], '@key' => $element_key];
      if (isset($element['#options'])) {
        $element_title = (isset($element['#title'])) ? new FormattableMarkup('@title (@key) - raw', $params) : $element_key;
        $options_element_options["[webform_submission:values:$element_key:raw]"] = $element_title;
      }

      // Multiple value elements can NOT be used as a tokens.
      if ($element_plugin->hasMultipleValues($element)) {
        continue;
      }

      $element_title = (isset($element['#title'])) ? new FormattableMarkup('@title (@key)', $params) : $element_key;
      $options['Elements']["[webform_submission:values:$element_key:value]"] = $element_title;
    }

    if ($type == 'mail') {
      $options['Other'] = [
        '[site:mail]' => 'Site email address',
        '[current-user:mail]' => 'Current user email address [Authenticated only]',
        '[webform:author:mail]' => 'Webform author email address',
        '[webform_submission:user:mail]' => 'Webform submission owner email address [Authenticated only]',
      ];
    }
    else {
      $options['Other'] = [
        '[site:name]' => 'Site name',
        '[current-user:display-name]' => 'Current user display name',
        '[current-user:account-name]' => 'Current user account name',
        '[webform:author:display-name]' => 'Webform author display name',
        '[webform:author:account-name]' => 'Webform author account name',
        '[webform_submission:user:display-name]' => 'Webform submission author display name',
        '[webform_submission:user:account-name]' => 'Webform submission author account name',
      ];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  protected function applyFormStateToConfiguration(FormStateInterface $form_state) {
    $form_state->setValue('type', $form_state->getValue(['thread_type', 'type']));
    $form_state->unsetValue(['thread_type']);
    $form_state->unsetValue(['recipients', 'add_recipient']);
    parent::applyFormStateToConfiguration($form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    $state = $webform_submission->getWebform()->getSetting('results_disabled') ? WebformSubmissionInterface::STATE_COMPLETED : $webform_submission->getState();
    if ($state != WebformSubmissionInterface::STATE_COMPLETED) {
      return;
    }

    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface $type */
    $type = $this->getEntityTypeManager()
      ->getStorage('webform_email_reply_thread_type')
      ->load($this->configuration['type']);
    if (!$this->threadTypeCreateAccessCheck->checkAccess($type)) {
      $context = [
        'submission' => $webform_submission->id(),
        'webform' => $webform_submission->getWebform()->id(),
      ];
      $this->getLogger('webform_email_reply_threads')
        ->warning('Skipped thread creation for submission {submission} on webform {webform}, access denied.', $context);
      return;
    }

    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread */
    $thread = $this->getEntityTypeManager()
      ->getStorage('webform_email_reply_thread')
      ->create([
        'type' => $type->id(),
        'submission' => $webform_submission,
      ]);
    $thread->save();

    // Set up sender and recipient values.
    $sender_mail = trim($this->replaceTokens($this->configuration['sender']['mail'], $webform_submission));
    $sender_name = trim($this->replaceTokens($this->configuration['sender']['name'], $webform_submission));
    $recipients = array_map(function ($recipient) use ($webform_submission) {
      $value = [
        'mail' => trim($this->replaceTokens($recipient['mail'], $webform_submission)),
        'name' => trim($this->replaceTokens($recipient['name'], $webform_submission)),
      ];
      if (empty($value['mail'])) {
        return FALSE;
      }
      return $value;
    }, $this->configuration['recipients']);
    $recipients = array_filter($recipients, function ($recipient) use ($thread) {
      if (!$this->getEmailValidator()->isValid($recipient['mail'])) {
        $text = $this->t('View thread @label', ['@label' => $thread->label()]);
        $context = [
          'mail' => $recipient['mail'],
          'link' => Link::fromTextAndUrl($text, $thread->toUrl())->toString(),
        ];
        $this->getLogger('webform_email_reply_threads')
          ->warning('Skipping new thread notification, invalid recipient email address "{mail}".', $context);
        return FALSE;
      }
      return TRUE;
    });

    // Bail if sender or recipient emails are invalid.
    $error = FALSE;
    $error_params = ['submission' => Link::fromTextAndUrl($webform_submission->label(), $webform_submission->toUrl())];
    if (!$this->getEmailValidator()->isValid($sender_mail)) {
      $this->getLogger()->error($this->t('Invalid sender email address webform submission @submission.', $error_params));
      $error = TRUE;
    }
    if (empty($recipients)) {
      $this->getLogger()->error($this->t('No valid recipient email addresses for webform submission @submission.', $error_params));
      $error = TRUE;
    }
    if ($error) {
      return;
    }

    // Track sender and recipients.
    $sender_id = $this->getThreadTracking()->addThreadRecipient($thread, ThreadTracking::ORIGINAL_SENDER, NULL, $sender_mail, $sender_name);
    foreach ($recipients as $recipient) {
      $this->getThreadTracking()->addThreadRecipient($thread, ThreadTracking::ORIGINAL_RECIPIENT, NULL, $recipient['mail'], $recipient['name'] ?: NULL);
    }

    // Send notifications.
    $this->getReplySender()->sendReply($thread, 'new_thread', $sender_id);
  }

}
