<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class WebformEmailReplyThreadHtmlRouteProvider extends AdminHtmlRouteProvider {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getCanonicalRoute(EntityTypeInterface $entity_type) {
    $entity_type_id = $entity_type->id();

    $route = new Route('/webform/{webform}/reply/{webform_email_reply_thread}/{uuid}');
    $route->setDefault('_entity_view', "{$entity_type_id}.full");

    // Optional, access check enforces this to be required for non-admins.
    $route->setDefault('uuid', NULL);

    // Overridden to thread label by entity rendering.
    $route->setDefault('_title', 'Reply');

    // Access check.
    $route->setRequirement('_webform_email_reply_thread', 'webform_email_reply_thread');

    $route->setOptions([
      '_admin_route' => TRUE,
      'parameters' => [
        'webform' => ['type' => 'entity:webform'],
        'webform_email_reply_thread' => [
          'type' => 'entity:webform_email_reply_thread',
        ],
      ],
    ]);
    $route->setOption('_admin_route', TRUE);

    return $route;
  }

}
