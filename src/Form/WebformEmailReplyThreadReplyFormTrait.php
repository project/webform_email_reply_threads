<?php

namespace Drupal\webform_email_reply_threads\Form;

use Drupal\awareness\Entity\EntityTypeManagerAwareTrait;
use Drupal\awareness\Session\CurrentUserAwareTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\webform_email_reply_threads\Service\ReplySenderAwareTrait;
use Drupal\webform_email_reply_threads\Service\ThreadTracking;
use Drupal\webform_email_reply_threads\Service\ThreadTrackingAwareTrait;
use Drupal\webform_email_reply_threads\Util\EmailAddressFormatter;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;
use Email\Parse;

/**
 * Trait for common webform email reply functionality.
 */
trait WebformEmailReplyThreadReplyFormTrait {

  use CurrentUserAwareTrait;
  use EntityTypeManagerAwareTrait;
  use ReplySenderAwareTrait;
  use ThreadTrackingAwareTrait;

  /**
   * Builds out the reply form.
   *
   * @return array
   *   The form element with the reply form build it.
   */
  protected function buildReplyForm(WebformEmailReplyThreadInterface $thread) {
    $current_recipient = $this->getThreadTracking()->getCurrentThreadRecipient($thread);

    $default_from_email = NULL;
    if ($current_recipient) {
      $default_from_email = EmailAddressFormatter::formatArray($current_recipient);
    }
    else {
      // No current recipient, use logged-in user.
      $account = $this->getCurrentUser();
      if ($account->isAuthenticated()) {
        $default_from_email = EmailAddressFormatter::format($this->getCurrentUser()->getEmail(), $this->getCurrentUser()->getDisplayName());
      }
    }

    $form['#thread'] = $thread;
    $form['#tree'] = TRUE;
    $form['#message_key'] = $thread->isNew() ? 'new_thread' : 'thread_reply';

    $recipients = $this->getThreadTracking()->getThreadRecipients($thread);

    $form['from_address'] = [
      '#type' => 'textfield',
      '#title' => t('From'),
      '#default_value' => $default_from_email,
      '#description' => t('The email address to send from.'),
      '#required' => TRUE,
      '#disabled' => $current_recipient,
      '#element_validate' => [
        [$this, 'validateParsedEmail'],
      ],
    ];

    $default_recipients = $recipients;
    if ($current_recipient) {
      $default_recipients = array_filter($default_recipients, function ($recipient) use ($current_recipient) {
        return $recipient['id'] != $current_recipient['id'];
      });
    }
    $default_recipients = array_map(function ($recipient) {
      return empty($recipient['status']) ? NULL : EmailAddressFormatter::formatArray($recipient);
    }, $default_recipients);
    $default_recipients = implode(', ', array_filter($default_recipients));

    $form['email'] = [
      '#type' => 'textfield',
      '#title' => t('To'),
      '#required' => TRUE,
      '#mulitple' => TRUE,
      '#maxlength' => NULL,
      '#default_value' => $default_recipients,
      '#disabled' => TRUE,
    ];

    $form['message'] = [
      '#type' => 'webform_html_editor',
      '#title' => t('Message'),
      '#required' => TRUE,
    ];

    $form['attachment'] = [
      '#type' => 'managed_file',
      '#title' => t("Attachment"),
      '#upload_location' => 'public://webform_email_reply/',
    ];

    return $form;
  }

  /**
   * Element validation callback for parsed email addresses.
   */
  public static function validateParsedEmail(array $element, FormStateInterface $form_state) {
    $value = $form_state->getValue($element['#parents']);
    $result = Parse::getInstance()->parse($value);

    if (!$result['success']) {
      $form_state->setErrorByName($element['#name'], $result['reason']);
      return;
    }

    // If required, ensure we've got at least one valid address.
    if ($element['#required'] && count($result['email_addresses']) < 1) {
      $params = ['@name' => $element['#title']];
      if (!empty($element['#multiple'])) {
        $form_state->setErrorByName($element['#name'], t('One or more valid email addresses are required for @name.', $params));
      }
      else {
        $form_state->setErrorByName($element['#name'], t('A valid email addresses is required for @name.', $params));
      }
      return;
    }

    // If not multiple, ensure we've only got a single value.
    if (!empty($element['#multiple']) && count($result['email_addresses']) > 1) {
      $params = ['@name' => $element['#title']];
      $form_state->setErrorByName($element['#name'], t('Only one email address allowed for @name.', $params));
    }

    // Allow message key to be passed in.
    if (!empty($element['#message_key'])) {
      $result['message_key'] = $element['#message_key'];
    }

    $form_state->set($element['#parents'], $result);
  }

  /**
   * Send a reply to a thread from a reply form.
   *
   * @param array $element
   *   Form element representing the reply form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   */
  protected function sendReply(array $element, FormStateInterface $form_state) {
    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread */
    $thread = $element['#thread'];

    // Get the sender.
    $from = $form_state->get(array_merge($element['#parents'], ['from_address', 'email_addresses', 0]));
    $message_key = $element['#message_key'];

    // Ensure the sender is added as a recipient on the thread.
    if ($message_key == 'new_thread') {
      $sender_type = ThreadTracking::ORIGINAL_SENDER;
    }
    else {
      $sender_type = ThreadTracking::OTHER_RECIPIENT;
    }

    // If needed, add the sender as a recipient.
    if ($sender = $this->getThreadTracking()->getCurrentThreadRecipient($thread)) {
      $sender_id = $sender['id'];
      $this->getThreadTracking()->activateRecipient($sender_id);
    }
    else {
      $sender_id = $this->getThreadTracking()->addThreadRecipient($thread, $sender_type, NULL, $from['simple_address'], $from['name_parsed'] ?: NULL);
    }

    // Get the attachment.
    $file = NULL;
    $fid = $form_state->getValue(array_merge($element['#parents'], ['attachment']));
    if ($fid = reset($fid)) {
      /** @var \Drupal\file\FileInterface $file */
      $file = $this->getEntityTypeManager()
        ->getStorage('file')
        ->load($fid);
    }

    // Send it.
    $this->getReplySender()->sendReply(
      $thread,
      $message_key,
      $sender_id,
      $form_state->getValue(array_merge($element['#parents'], ['message'])),
      $file
    );

    $this->messenger()->addStatus('Reply sent.');
  }

}
