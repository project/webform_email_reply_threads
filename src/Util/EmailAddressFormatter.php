<?php

namespace Drupal\webform_email_reply_threads\Util;

/**
 * Formats email addresses.
 */
class EmailAddressFormatter {

  /**
   * Formats RFC 822 email address header.
   *
   * @param string $mail
   *   The email address.
   * @param string|null $name
   *   The name, if applicable.
   *
   * @return string
   *   The formatted address.
   */
  public static function format(string $mail, string|null $name = NULL): string {
    if ($name) {
      if (preg_match('/[\s,]/', $name)) {
        $name = "\"$name\"";
      }
      return "$name <$mail>";
    }
    return $mail;
  }

  /**
   * Formats RFC 822 email address header.
   *
   * @param array $value
   *   An array containing a "mail" element, and an optional "name" element.
   *
   * @return string
   *   The formatted address.
   */
  public static function formatArray(array $value): string {
    return static::format($value['mail'], $value['name'] ?? NULL);
  }

}
