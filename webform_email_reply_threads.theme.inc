<?php

/**
 * @file
 * Webform Email Reply Threads theme hooks.
 */

use Drupal\Component\Utility\Html;
use Drupal\file\Entity\File;

/**
 * Implements template_preprocess_HOOK().
 */
function template_preprocess_webform_email_reply_threads_reply(&$variables) {
  $variables['from'] = $variables['reply']['#reply']['from_address'];

  /** @var \Drupal\Core\Datetime\DateFormatterInterface $df */
  $df = \Drupal::service('date.formatter');
  $variables['sent'] = $df->format($variables['reply']['#reply']['replied']);

  if ($variables['reply']['#reply']['fid']) {
    if ($file = File::load($variables['reply']['#reply']['fid'])) {
      $variables['attachment'] = [
        '#theme' => 'file_link',
        '#file' => $file,
      ];
    }
  }

  $variables['message'] = [
    '#theme' => 'webform_html_editor_markup',
    '#markup' => $variables['reply']['#reply']['message'],
  ];

  $variables['attributes']['class'][] = Html::getClass('webform-email-reply');
}
