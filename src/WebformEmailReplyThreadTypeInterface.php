<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a webform email reply thread type entity type.
 */
interface WebformEmailReplyThreadTypeInterface extends ConfigEntityInterface {

}
