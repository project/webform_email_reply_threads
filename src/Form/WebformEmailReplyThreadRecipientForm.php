<?php

namespace Drupal\webform_email_reply_threads\Form;

use Drupal\awareness\Mail\MailPluginManagerAwareTrait;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\webform_email_reply_threads\Service\ThreadTracking;
use Drupal\webform_email_reply_threads\Service\ThreadTrackingAwareTrait;
use Drupal\webform_email_reply_threads\Util\EmailAddressFormatter;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;
use Email\Parse;

/**
 * Form for managing recipients on a thread.
 */
class WebformEmailReplyThreadRecipientForm extends FormBase {

  use MailPluginManagerAwareTrait;
  use ThreadTrackingAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'webform_email_reply_thread_recipient_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, WebformEmailReplyThreadInterface $thread = NULL) {
    if (!$thread) {
      return $form;
    }

    $form['#thread'] = $thread;

    $recipients = $this->getThreadTracking()->getThreadRecipients($thread);

    $form['#tree'] = TRUE;
    $recipient_count = count(array_filter($recipients, function ($recipient) {
      return $recipient['type'] != ThreadTracking::ORIGINAL_SENDER;
    }));

    foreach ($recipients as $id => $recipient) {
      $original_sender = $recipient['type'] == ThreadTracking::ORIGINAL_SENDER;
      $form['recipients'][$id] = [
        '#type' => 'container',
        'mail' => [
          '#type' => 'textfield',
          '#title' => $original_sender ? $this->t('Original sender') : $this->t('Recipient'),
          '#disabled' => TRUE,
          '#value' => EmailAddressFormatter::formatArray($recipient),
        ],
        'remove' => [
          '#type' => 'submit',
          '#value' => $this->t('Remove'),
          '#name' => "remove_{$id}",
          '#disabled' => $original_sender || $recipient_count < 2,
          '#op' => 'remove',
          '#op_id' => $id,
        ],
      ];
    }

    $form['add'] = [
      '#type' => 'container',
      'mail' => [
        '#type' => 'textfield',
        '#title' => $this->t('Add recipient'),
      ],
      'add' => [
        '#type' => 'submit',
        '#value' => $this->t('Add'),
        '#op' => 'add',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread */
    $thread = $form['#thread'];

    $recipients = $this->getThreadTracking()->getThreadRecipients($thread);

    $op = $form_state->getTriggeringElement()['#op'] ?? NULL;
    switch ($op) {
      case 'add';
        $mail = $form_state->getValue(['add', 'mail']);

        // Ensure the email address is present, and valid.
        $result = Parse::getInstance()->parse($mail, FALSE);
        if ($result['invalid']) {
          $form_state->setError($form['add']['mail'], $this->t('A valid email address is required.'));
          return;
        }

        // Ensure the email address isn't already used as a recipient.
        foreach ($recipients as $recipient) {
          if (strtolower($recipient['mail']) == strtolower($mail)) {
            $params = ['%mail' => $mail];
            $form_state->setError($form['add']['mail'], $this->t('The address %mail is already a recipient for this thread.', $params));
            return;
          }
        }
        break;

      case 'remove';
        // Ensure that removing the recipient does not leave us without the
        // original sender and at least one other recipient. Much or all of this
        // should be handled via FAPI on the front end, but we'll check here
        // just in case.
        $original_sender = 0;
        $other_recipients = 0;
        $remove_id = $form_state->getTriggeringElement()['#op_id'];
        foreach ($recipients as $recipient) {
          if ($recipient['id'] == $remove_id) {
            continue;
          }
          if ($recipient['type'] == ThreadTracking::ORIGINAL_SENDER) {
            $original_sender++;
          }
          else {
            $other_recipients++;
          }
        }
        if (!$original_sender || !$other_recipients) {
          $form_state->setError($form['recipients'][$remove_id]['mail'], $this->t('This recipient cannot be removed.'));
        }
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread */
    $thread = $form['#thread'];

    $recipients = $this->getThreadTracking()->getThreadRecipients($thread, FALSE);

    $op = $form_state->getTriggeringElement()['#op'] ?? NULL;
    switch ($op) {
      case 'add';
        $mail = $form_state->getValue(['add', 'mail']);
        $recipient_id = NULL;
        foreach ($recipients as $recipient) {
          if (strtolower($recipient['mail']) == strtolower($mail)) {
            $recipient_id = $recipient['id'];
          }
        }

        // Enable existing recipient, or add new.
        if ($recipient_id) {
          $this->getThreadTracking()->activateRecipient((int) $recipient_id);
        }
        else {
          $result = Parse::getInstance()->parse($mail, FALSE);
          $recipient_id = $this->getThreadTracking()->addThreadRecipient($thread, ThreadTracking::OTHER_RECIPIENT, NULL, $result['simple_address'], $result['name_parsed'] ?: NULL);
        }

        // Reload the recipients.
        $recipients = $this->getThreadTracking()->getThreadRecipients($thread, FALSE);
        $recipient = $recipients[$recipient_id];

        $params = [
          'subject' => $this->t('Subscribed to thread :label', [
            ':label' => $thread->label(),
          ]),
          'thread' => $thread,
          'recipient' => $recipient,
        ];
        $this->getMailPluginManager()->mail('webform_email_reply_threads', 'recipient_subscribe', $recipient['mail'], LanguageInterface::LANGCODE_NOT_SPECIFIED, $params);

        $this->messenger()->addStatus($this->t('Recipient added.'));
        break;

      case 'remove':
        $remove_id = $form_state->getTriggeringElement()['#op_id'];
        $recipient = $recipients[$remove_id];
        $this->getThreadTracking()->deActivateRecipient((int) $remove_id);

        $params = [
          'subject' => $this->t('Unsubscribed from thread :label', [
            ':label' => $thread->label(),
          ]),
          'thread' => $thread,
          'recipient' => $recipient,
        ];
        $this->getMailPluginManager()->mail('webform_email_reply_threads', 'recipient_unsubscribe', $recipient['mail'], LanguageInterface::LANGCODE_NOT_SPECIFIED, $params);

        $this->messenger()->addStatus($this->t('Recipient removed.'));
        break;
    }

  }

}
