<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Controller;

use Drupal\awareness\DateTime\DateFormatterAwareTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_email_reply_threads\Service\ThreadTrackingAwareTrait;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface;

/**
 * Returns responses for Webform Email Reply Threads routes.
 */
class ThreadsController extends ControllerBase {

  use DateFormatterAwareTrait;
  use ThreadTrackingAwareTrait;

  /**
   * Generate the listing of threads for a webform.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The submission.
   *
   * @return array
   *   The page content.
   */
  public function submissionThreadsPage(WebformSubmissionInterface $webform_submission): array {
    $threads = $this->getThreadTracking()
      ->getSubmissionThreads($webform_submission);

    $build['table'] = [
      '#theme' => 'table',
      '#header' => [
        $this->t('Thread'),
        $this->t('Updated'),
      ],
      '#rows' => [],
      '#empty' => $this->t('No threads.'),
    ];

    foreach ($threads as $thread) {
      $row = [
        Link::fromTextAndUrl($thread->label(), $thread->toUrl()),
        $this->getDateFormatter()->format($thread->getChangedTime()),
      ];
      $build['table']['#rows'][] = $row;
    }

    return $build;
  }

  /**
   * Generate the page listing thread types available for creation.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The submission.
   *
   * @return array
   *   The page content.
   */
  public function threadBundleListingPage(WebformSubmissionInterface $webform_submission): array {
    $build = [];

    // Local tasks for managing types.
    $types = $this->entityTypeManager()
      ->getStorage('webform_email_reply_thread_type')
      ->loadMultiple();

    $definition = $this->entityTypeManager()
      ->getDefinition('webform_email_reply_thread_type');
    uasort($types, [$definition->getClass(), 'sort']);

    $content = [];
    foreach ($types as $type) {
      $url_params = [
        'webform' => $webform_submission->getWebform()->id(),
        'webform_submission' => $webform_submission->id(),
        'webform_email_reply_thread_type' => $type->id(),
      ];
      $url = Url::fromRoute('entity.webform_email_reply_thread.create.bundle', $url_params);
      if (!$url->access()) {
        continue;
      }

      $description_params = ['%type' => $type->label()];
      $item = [
        'title' => $type->label(),
        'description' => $this->t('Create a %type webform reply thread.', $description_params),
        'url' => $url,
      ];
      $content[] = $item;
    }

    $build['types'] = [
      '#theme' => 'admin_block_content',
      '#content' => $content,
    ];

    $manage_params = [
      '@link' => Link::fromTextAndUrl($this->t('here'), Url::fromRoute('entity.webform_email_reply_thread_type.collection'))->toString(),
    ];
    $build['manage']['#markup'] = $this->t('Click @link to manage thread types.', $manage_params);

    return $build;
  }

  /**
   * Generate a thread creation page.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The submission that the thread is being created for.
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface $webform_email_reply_thread_type
   *   The thread type for the thread to be created.
   *
   * @return array
   *   The page content.
   */
  public function threadCreatePage(WebformSubmissionInterface $webform_submission, WebformEmailReplyThreadTypeInterface $webform_email_reply_thread_type): array {
    $build = [];

    $thread = $this->entityTypeManager()
      ->getStorage('webform_email_reply_thread')
      ->create([
        'type' => $webform_email_reply_thread_type->id(),
        'submission' => $webform_submission,
      ]);

    $build['form'] = $this->entityFormBuilder()->getForm($thread, 'add');

    return $build;
  }

}
