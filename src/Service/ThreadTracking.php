<?php

namespace Drupal\webform_email_reply_threads\Service;

use Drupal\awareness\Database\DatabaseAwareTrait;
use Drupal\awareness\DateTime\TimeAwareTrait;
use Drupal\awareness\Entity\EntityTypeManagerAwareTrait;
use Drupal\awareness\Extension\ModuleHandlerAwareTrait;
use Drupal\awareness\Routing\RouteMatchAwareTrait;
use Drupal\awareness\Session\CurrentUserAwareTrait;
use Drupal\awareness\Uuid\UuidAwareTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;

/**
 * Track webform email replies to threads.
 */
class ThreadTracking {

  use CurrentUserAwareTrait;
  use DatabaseAwareTrait;
  use EntityTypeManagerAwareTrait;
  use ModuleHandlerAwareTrait;
  use RouteMatchAwareTrait;
  use TimeAwareTrait;
  use UuidAwareTrait;

  const OTHER_RECIPIENT = 0;

  const ORIGINAL_SENDER = 1;

  const ORIGINAL_RECIPIENT = 2;

  /**
   * Get threads for a webform submission.
   *
   * @return \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface[]
   *   The threads for the submission.
   */
  public function getSubmissionThreads(WebformSubmissionInterface $submission) : array {
    $q = $this->getEntityQuery('webform_email_reply_thread');
    $q->condition(('submission.target_id'), $submission->id());
    $thread_ids = $q->sort('created')
      ->accessCheck(FALSE)
      ->execute();
    if (!$thread_ids) {
      return [];
    }
    return $this->getEntityTypeManager()
      ->getStorage('webform_email_reply_thread')
      ->loadMultiple($thread_ids);
  }

  /**
   * Record a reply as belonging to a thread.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread ID.
   * @param int $reply_id
   *   The reply ID.
   * @param int $sender_id
   *   The recipient ID of the sender.
   */
  public function recordThreadReply(WebformEmailReplyThreadInterface $thread, int $reply_id, int $sender_id) {
    $this->getDatabase()->insert('webform_email_reply_thread_replies')
      ->fields([
        'thread_id' => $thread->id(),
        'reply_id' => $reply_id,
        'sender' => $sender_id,
      ])->execute();
    $thread->setLastActivity($this->getTime()->getCurrentTime())->save();
    $reply = $this->getReplyById($reply_id);
    $this->getModuleHandler()
      ->invokeAll('webform_email_reply_threads_reply', [
        $thread,
        $reply,
      ]);
  }

  /**
   * Add a recipient to a thread.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param int $type
   *   Recipient type. Possible values are 1 (original sender), 2 (original
   *   recipient), or 0 (other).
   * @param int|null $uid
   *   The recipient's user ID.
   * @param string|null $mail
   *   The recipient's email address.
   * @param string|null $name
   *   The recipient's name.
   *
   * @return int
   *   The tracking ID of the recipient.
   */
  public function addThreadRecipient(WebformEmailReplyThreadInterface $thread, int $type, int $uid = NULL, string $mail = NULL, string $name = NULL): int {
    // Attempt to set the UID.
    if ($uid === NULL) {
      $ids = $this->getEntityQuery('user')
        ->condition('mail', $mail)
        ->accessCheck(FALSE)
        ->execute();
      $uid = reset($ids);
    }

    if ($uid) {
      /** @var \Drupal\user\UserInterface $account */
      $account = $this->getEntityTypeManager()->getStorage('user')->load($uid);
      $name = $account->getDisplayName();
      $mail = $account->getEmail();
    }

    $recipients = $this->getThreadRecipients($thread, FALSE);

    // If matching UID, update mail and name.
    if ($uid) {
      $uid_recipient = array_filter($recipients, function ($recipient) use ($uid) {
        return $recipient['uid'] == $uid;
      });
      $uid_recipient = reset($uid_recipient);
      if ($uid_recipient) {
        $this->getDatabase()
          ->update('webform_email_reply_thread_recipients')
          ->condition('id', $uid_recipient['id'])
          ->fields([
            'mail' => $mail,
            'name' => $name,
          ])
          ->execute();
        return $uid_recipient['id'];
      }
    }

    // If matching email address, update name.
    $mail_recipient = array_filter($recipients, function ($recipient) use ($mail) {
      return $recipient['mail'] == $mail;
    });
    $mail_recipient = reset($mail_recipient);
    if ($mail_recipient) {
      if ($name !== NULL && $mail_recipient['name'] != $name) {
        $this->getDatabase()
          ->update('webform_email_reply_thread_recipients')
          ->condition('id', $mail_recipient['id'])
          ->fields([
            'name' => $name,
          ])
          ->execute();
      }
      return $mail_recipient['id'];
    }

    // Otherwise, just insert.
    $values = [
      'thread_id' => $thread->id(),
      'type' => $type,
      'uid' => $uid ?: NULL,
      'mail' => $mail,
      'name' => $name ?: NULL,
      'uuid' => $this->getUuid()->generate(),
    ];
    return $this->getDatabase()
      ->insert('webform_email_reply_thread_recipients')
      ->fields($values)
      ->execute();
  }

  /**
   * Get recipient data by ID.
   *
   * @param int $id
   *   The recipient ID.
   *
   * @return array|null
   *   The recipient data, or NULL if not found.
   */
  public function getRecipientById(int $id) {
    return $this->getDatabase()
      ->select('webform_email_reply_thread_recipients', 'r')
      ->fields('r')
      ->condition('id', $id)
      ->range(0, 1)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
  }

  /**
   * Get reply data by ID.
   *
   * @param int $reply_id
   *   The reply ID.
   *
   * @return array|null
   *   The reply data, or NULL if not found.
   */
  public function getReplyById(int $reply_id) {
    $reply = $this->getDatabase()
      ->select('webform_email_reply', 'r')
      ->fields('r')
      ->condition('eid', $reply_id)
      ->range(0, 1)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
    if (!$reply) {
      return NULL;
    }

    $reply['meta'] = $this->getReplyMetaData($reply_id);

    return $reply;
  }

  /**
   * Get thread reply metadata.
   *
   * @param int $reply_id
   *   The reply ID.
   *
   * @return array|null
   *   The metadata, or NULL if not found.
   */
  public function getReplyMetaData(int $reply_id) {
    return $this->getDatabase()
      ->select('webform_email_reply_thread_replies', 'r')
      ->fields('r')
      ->condition('reply_id', $reply_id)
      ->range(0, 1)
      ->execute()
      ->fetch(\PDO::FETCH_ASSOC);
  }

  /**
   * Gets recipients for the given thread.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param bool $active_only
   *   Loads only active recipients. Optional, defaults to TRUE.
   *
   * @return array
   *   Thread recipient data.
   */
  public function getThreadRecipients(WebformEmailReplyThreadInterface $thread, bool $active_only = TRUE) {
    $query = $this->getDatabase()
      ->select('webform_email_reply_thread_recipients', 'r')
      ->fields('r')
      ->condition('thread_id', $thread->id());

    if ($active_only) {
      $query->condition('status', 1);
    }

    $recipients = $query->execute()
      ->fetchAllAssoc('id', \PDO::FETCH_ASSOC);

    if ($uids = array_filter(array_column($recipients, 'uid'))) {
      /** @var \Drupal\user\UserInterface[] $users */
      $users = $this->getEntityTypeManager()
        ->getStorage('user')
        ->loadMultiple($uids);
      foreach ($recipients as $id => $recipient) {
        if (array_key_exists($recipient['uid'], $users)) {
          $recipients[$id]['mail'] = $users[$recipient['uid']]->getEmail();
          $recipients[$id]['name'] = $users[$recipient['uid']]->getDisplayName();
        }
      }
    }

    return $recipients;
  }

  /**
   * Get the current user's recipient data for a given thread.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param string|null $uuid
   *   UUID if available.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user. Optional, defaults to current user if omitted.
   *
   * @return array|null
   *   The recipient data, or NULL if not available.
   */
  public function getCurrentThreadRecipient(WebformEmailReplyThreadInterface $thread, $uuid = NULL, AccountInterface $account = NULL): array|null {
    if (!$account) {
      $account = $this->getCurrentUser();
    }

    if ($uuid === NULL) {
      $uuid = !$thread->isNew() ? $this->getRouteMatch()->getParameter('uuid') : NULL;
    }

    $recipients = $this->getThreadRecipients($thread, FALSE);

    $mail_match = $uuid_match = [];
    foreach ($recipients as $recipient) {
      if ($account->isAuthenticated() && ($recipient['uid'] == $account->id())) {
        return $recipient;
      }
      if ($recipient['mail'] == $account->getEmail()) {
        if (!$mail_match) {
          $mail_match = $recipient;
        }
      }
      if ($recipient['uuid'] == $uuid) {
        if (!$uuid_match) {
          $uuid_match = $recipient;
        }
      }
    }

    if ($mail_match) {
      return $mail_match;
    }

    return $uuid_match;
  }

  /**
   * Get webform email replies for a thread.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   *
   * @return array
   *   Webform email replies for the thread.
   */
  public function getThreadReplies(WebformEmailReplyThreadInterface $thread): array {
    $query = $this->getDatabase()
      ->select('webform_email_reply', 'r')
      ->fields('r');

    $query->innerJoin('webform_email_reply_thread_replies', 'tr', 'tr.reply_id = r.eid');

    return $query->condition('tr.thread_id', $thread->id())
      ->orderBy('r.replied')
      ->execute()
      ->fetchAll(\PDO::FETCH_ASSOC);
  }

  /**
   * Delete replies and tracking for a given thread.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   */
  public function deleteThreadReplyTracking(WebformEmailReplyThreadInterface $thread) {
    // Query replies for the thread.
    $reply_ids = $this->getDatabase()
      ->select('webform_email_reply_thread_replies', 'tr')
      ->fields('tr', ['reply_id'])
      ->condition('thread_id', $thread->id())
      ->execute()
      ->fetchCol();
    if ($reply_ids) {
      // Delete files.
      $fids = $this->getDatabase()
        ->select('webform_email_reply', 'r')
        ->fields('r', ['fid'])
        ->condition('eid', $reply_ids, 'IN')
        ->execute()
        ->fetchCol();
      $fids = array_filter($fids);
      if ($fids) {
        $files = $this->getEntityTypeManager()
          ->getStorage('file')
          ->loadMultiple($fids);
        if ($files) {
          $this->getEntityTypeManager()
            ->getStorage('file')
            ->delete($files);
        }
      }
      // Delete the replies.
      $this->getDatabase()
        ->delete('webform_email_reply')
        ->condition('eid', $reply_ids, 'IN')
        ->execute();
    }

    // Delete thread/reply tracking.
    $this->getDatabase()
      ->delete('webform_email_reply_thread_replies')
      ->condition('thread_id', $thread->id())
      ->execute();

    // Delete thread recipient tracking.
    $this->getDatabase()
      ->delete('webform_email_reply_thread_recipients')
      ->condition('thread_id', $thread->id())
      ->execute();
  }

  /**
   * Set a recipient UID.
   *
   * @param int $recipient_id
   *   The recipient record ID.
   * @param int $uid
   *   The user ID.
   */
  public function setRecipientUid(int $recipient_id, int $uid) {
    $this->getDatabase()
      ->update('webform_email_reply_thread_recipients')
      ->fields(['uid' => $uid])
      ->condition('id', $recipient_id)
      ->execute();
  }

  /**
   * Activate a recipient.
   *
   * @param int $recipient_id
   *   The recipient record ID.
   */
  public function activateRecipient(int $recipient_id) {
    $this->getDatabase()
      ->update('webform_email_reply_thread_recipients')
      ->fields(['status' => 1])
      ->condition('id', $recipient_id)
      ->execute();
  }

  /**
   * Deactivate a recipient.
   *
   * @param int $recipient_id
   *   The recipient record ID.
   */
  public function deActivateRecipient(int $recipient_id) {
    $this->getDatabase()
      ->update('webform_email_reply_thread_recipients')
      ->fields(['status' => 0])
      ->condition('id', $recipient_id)
      ->execute();
  }

}
