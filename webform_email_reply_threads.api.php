<?php

/**
 * @file
 * Hooks provided by the Webform Email Reply Threads module.
 */

use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;

/**
 * Allows other modules to act on the creation of a reply to a thread.
 *
 * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
 *   The thread.
 * @param array $reply
 *   The reply.
 */
function hook_webform_email_reply_threads_reply(WebformEmailReplyThreadInterface $thread, array $reply) {
  // If for some reason we wanted to track the ID of the last reply, we could
  // have a custom field on the thread type and populate it as replies are
  // created.
  $thread->field_last_reply = $reply['id'];
  $thread->save();
}
