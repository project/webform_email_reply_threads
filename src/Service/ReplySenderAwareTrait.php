<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Service;

/**
 * Trait to utilize the webform_email_reply_threads.reply_sender service.
 */
trait ReplySenderAwareTrait {

  /**
   * Get the webform_email_reply_threads.reply_sender service.
   *
   * @return \Drupal\webform_email_reply_threads\Service\ReplySender
   *   The webform_email_reply_threads.reply_sender service.
   */
  protected function getReplySender(): ReplySender {
    return \Drupal::service('webform_email_reply_threads.reply_sender');
  }

}
