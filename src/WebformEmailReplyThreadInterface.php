<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\webform\WebformSubmissionInterface;

/**
 * Provides an interface defining a webform email reply thread entity type.
 */
interface WebformEmailReplyThreadInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the thread creation timestamp.
   *
   * @return int
   *   Creation timestamp of the thread.
   */
  public function getCreatedTime();

  /**
   * Gets the thread's submission.
   *
   * @return \Drupal\webform\WebformSubmissionInterface|null
   *   The submission.
   */
  public function getSubmission(): ?WebformSubmissionInterface;

  /**
   * Get the last activity timestamp.
   *
   * @return int|null
   *   The last activity timestamp, or NULL if not set.
   */
  public function getLastActivity(): int|null;

  /**
   * Set the last activity timestamp.
   *
   * @param int $value
   *   The last activity timestamp.
   *
   * @return $this
   */
  public function setLastActivity(int $value): static;

  /**
   * Get the default subject.
   *
   * @return string
   *   The default subject.
   */
  public function getDefaultSubject();

}
