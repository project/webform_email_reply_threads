<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * Usage example:
 * @code
 * foo.example:
 *   path: '/thread/create/{type_param}'
 *   defaults:
 *     _title: 'Example'
 *     _controller:
 *   '\Drupal\webform_email_reply_threads\Controller\WebformEmailReplyThreadsController'
 *   requirements:
 *     _webform_email_reply_thread_type_create: 'type_param'
 * @endcode
 */
class WebformEmailReplyThreadTypeCreateAccess implements AccessInterface {

  /**
   * Access callback.
   */
  public function access(RouteMatchInterface $routeMatch): AccessResult {
    $requirement = $routeMatch->getRouteObject()->getRequirement('_webform_email_reply_thread_type_create');

    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface $type */
    if (!$type = $routeMatch->getParameter($requirement)) {
      return AccessResult::forbidden('No thread type provided by route.')
        ->addCacheTags(['webform_email_reply_thread:list']);
    }

    return AccessResult::allowedIf($this->checkAccess($type))
      ->addCacheableDependency($type);
  }

  /**
   * Determine access.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface $type
   *   The thread type.
   *
   * @return bool
   *   Indicates if access is granted.
   */
  public function checkAccess(WebformEmailReplyThreadTypeInterface $type): bool {
    return !empty($type->status());
  }

}
