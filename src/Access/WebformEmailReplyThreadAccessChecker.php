<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Access;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\webform\WebformInterface;
use Drupal\webform_email_reply_threads\Service\ThreadTrackingAwareTrait;
use Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface;
use Symfony\Component\Routing\Route;

/**
 * Checks if passed parameter matches the route configuration.
 *
 * Usage example:
 * @code
 * foo.example:
 *   path: '/webform/{webform}/view-replies/{thread_parameter_name}'
 *   defaults:
 *     _title: 'Example'
 *     _controller:
 *   '\Drupal\webform_email_reply_threads\Controller\WebformEmailReplyThreadsController'
 *   requirements:
 *     _webform_email_reply_thread: 'thread_parameter_name'
 * @endcode
 */
class WebformEmailReplyThreadAccessChecker implements AccessInterface {

  use ThreadTrackingAwareTrait;

  /**
   * Check route access.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account): AccessResult {
    // Ensure the thread is for a submission on the specified webform.
    $webform = $route_match->getParameter('webform');
    assert($webform instanceof WebformInterface);
    $thread = $route_match->getParameter($route->getRequirement('_webform_email_reply_thread'));
    assert($thread instanceof WebformEmailReplyThreadInterface);

    // Allowed via permission.
    $perm = AccessResult::allowedIfHasPermission($account, 'administer webform_email_reply_threads');

    // Recipient access.
    $uuid = $route_match->getParameter('uuid');
    $recipient_access = AccessResult::allowedIf($this->checkAccess($thread, $account, $uuid))
      ->addCacheContexts(['url.path'])
      ->addCacheableDependency($account)
      ->addCacheableDependency($thread);

    return $perm->orIf($recipient_access);
  }

  /**
   * Determine if access should be granted.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user attempting to access the thread.
   * @param string|null $uuid
   *   The UUID, or NULL if not provided.
   *
   * @return bool
   *   *   Indicates if access should be granted.
   */
  public function checkAccess(WebformEmailReplyThreadInterface $thread, AccountInterface $account, string|null $uuid) {
    if ($account->isAnonymous()) {
      $match = $uuid && $this->anonymousAccess($thread, $uuid);
    }
    else {
      $match = $this->authenticatedAccess($thread, $account, $uuid);
    }
    return $match;
  }

  /**
   * Determine if anonymous access should be granted.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param string $uuid
   *   The UUID.
   *
   * @return bool
   *   Indicates if anonymous access should be granted.
   */
  protected function anonymousAccess(WebformEmailReplyThreadInterface $thread, string $uuid): bool {
    $recipients = $this->getThreadTracking()->getThreadRecipients($thread, FALSE);
    foreach ($recipients as $recipient) {
      // If the recipient has a value for uid, the user must be logged-in to
      // access.
      if ($recipient['uid'] !== NULL) {
        continue;
      }
      if ($recipient['uuid'] == $uuid) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Determine if authenticated access should be granted.
   *
   * @param \Drupal\webform_email_reply_threads\WebformEmailReplyThreadInterface $thread
   *   The thread.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user attempting to access the thread.
   * @param string|null $uuid
   *   The UUID, or NULL if not provided.
   *
   * @return bool
   *   *   Indicates if authenticated access should be granted.
   */
  protected function authenticatedAccess(WebformEmailReplyThreadInterface $thread, AccountInterface $account, string|null $uuid) {
    $recipients = $this->getThreadTracking()->getThreadRecipients($thread, FALSE);

    foreach ($recipients as $recipient) {
      // Match on UUID.
      if ($uuid !== NULL && $uuid == $recipient['uuid']) {
        return TRUE;
      }

      // Match on user ID.
      if ($recipient['uid']) {
        if ($recipient['uid'] == $account->id()) {
          return TRUE;
        }
      }
    }

    return FALSE;
  }

}
