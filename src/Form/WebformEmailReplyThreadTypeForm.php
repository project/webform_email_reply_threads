<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Form;

use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Webform Email Reply Thread Type form.
 */
final class WebformEmailReplyThreadTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\webform_email_reply_threads\WebformEmailReplyThreadTypeInterface $type */
    $type = $this->entity;

    if ($this->operation != 'add') {
      $form['#title'] = $this->t('Edit %label webform email reply thread type', ['%label' => $type->label()]);
    }

    $form['label'] = [
      '#title' => $this->t('Label'),
      '#type' => 'textfield',
      '#default_value' => $type->label(),
      '#description' => $this->t('The human-readable name for this webform email reply thread type.'),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#disabled' => !$type->isNew(),
      '#machine_name' => [
        'exists' => ['Drupal\webform_email_reply_threads\Entity\WebformEmailReplyThreadType', 'load'],
        'source' => ['label'],
      ],
      '#description' => $this->t('Unique machine-readable name: lowercase letters, numbers, and underscores only.'),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Active'),
      '#description' => $this->t('Creating new threads or replying to existing threads is limited to active thread types.'),
      '#default_value' => $this->entity->status(),
    ];

    $form['default_subject'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default subject'),
      '#description' => $this->t('Default subject/name for threads of this type. Webform and webform submission token replacement is available for this value.'),
      '#default_value' => $type->get('default_subject'),
    ];

    $form['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        'webform',
        'webform_submission',
      ],
    ];

    $form['messages'] = [
      '#title' => $this->t('Messages'),
      '#type' => 'fieldset',
      '#tree' => TRUE,
    ];

    $form['messages']['tokens'] = [
      '#theme' => 'token_tree_link',
      '#token_types' => [
        'webform_email_reply_thread',
        'webform_email_reply_thread_recipient',
        'webform_email_reply_thread_reply',
        'webform_submission',
      ],
    ];

    $form['messages']['new_thread_recipient'] = [
      '#title' => $this->t('New Thread - Recipient'),
      '#description' => $this->t('Email sent to the recipient when a new thread is created.'),
      '#type' => 'fieldset',
    ];

    $form['messages']['new_thread_recipient']['status'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $type->get('messages')['new_thread_recipient']['status'] ?? TRUE,
    ];

    $form['messages']['new_thread_recipient']['message'] = [
      '#type' => 'text_format',
      '#default_value' => $type->get('messages')['new_thread_recipient']['message']['value'] ?? NULL,
      '#format' => $type->get('messages')['new_thread_recipient']['message']['format'] ?? NULL,
    ];

    $form['messages']['new_thread_sender'] = [
      '#title' => $this->t('New Thread - Sender'),
      '#description' => $this->t('Email sent to the sender when a new thread is created.'),
      '#type' => 'fieldset',
    ];

    $form['messages']['new_thread_sender']['status'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $type->get('messages')['new_thread_sender']['status'] ?? TRUE,
    ];

    $form['messages']['new_thread_sender']['message'] = [
      '#type' => 'text_format',
      '#default_value' => $type->get('messages')['new_thread_sender']['message']['value'] ?? NULL,
      '#format' => $type->get('messages')['new_thread_sender']['message']['format'] ?? NULL,
    ];

    $form['messages']['thread_reply'] = [
      '#title' => $this->t('Reply'),
      '#description' => $this->t('Email sent when a thread is replied to.'),
      '#type' => 'fieldset',
    ];

    $form['messages']['thread_reply']['status'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $type->get('messages')['thread_reply']['status'] ?? TRUE,
    ];

    $form['messages']['thread_reply']['message'] = [
      '#type' => 'text_format',
      '#default_value' => $type->get('messages')['thread_reply']['message']['value'] ?? NULL,
      '#format' => $type->get('messages')['thread_reply']['message']['format'] ?? NULL,
    ];

    $form['messages']['recipient_subscribe'] = [
      '#title' => $this->t('Recipient subscribe'),
      '#description' => $this->t('Email sent when a recipient is added to a thread.'),
      '#type' => 'fieldset',
    ];

    $form['messages']['recipient_subscribe']['status'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $type->get('messages')['recipient_subscribe']['status'] ?? TRUE,
    ];

    $form['messages']['recipient_subscribe']['message'] = [
      '#type' => 'text_format',
      '#default_value' => $type->get('messages')['recipient_subscribe']['message']['value'] ?? NULL,
      '#format' => $type->get('messages')['recipient_subscribe']['message']['format'] ?? NULL,
    ];

    $form['messages']['recipient_unsubscribe'] = [
      '#title' => $this->t('Recipient unsubscribe'),
      '#description' => $this->t('Email sent when a recipient is removed from a thread.'),
      '#type' => 'fieldset',
    ];

    $form['messages']['recipient_unsubscribe']['status'] = [
      '#title' => $this->t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $type->get('messages')['recipient_unsubscribe']['status'] ?? TRUE,
    ];

    $form['messages']['recipient_unsubscribe']['message'] = [
      '#type' => 'text_format',
      '#default_value' => $type->get('messages')['recipient_unsubscribe']['message']['value'] ?? NULL,
      '#format' => $type->get('messages')['recipient_unsubscribe']['message']['format'] ?? NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);

    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new thread type %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated thread type %label.', $message_args),
      }
    );

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  protected function prepareEntity() {
    parent::prepareEntity();

    // If we can load the "default" thread type, use it.
    if ($this->entity->isNew()) {
      $default = $this->entityTypeManager->getStorage('webform_email_reply_thread_type')
        ->load('default');
      if ($default) {
        $this->entity = $default->createDuplicate();
      }
    }
  }

}
