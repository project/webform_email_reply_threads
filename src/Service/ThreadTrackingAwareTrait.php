<?php

declare(strict_types=1);

namespace Drupal\webform_email_reply_threads\Service;

/**
 * Trait to utilize the webform_email_reply_threads.thread_tracking service.
 */
trait ThreadTrackingAwareTrait {

  /**
   * Get the webform_email_reply_threads.thread_tracking service.
   *
   * @return \Drupal\webform_email_reply_threads\Service\ThreadTracking
   *   The webform_email_reply_threads.thread_tracking service.
   */
  protected function getThreadTracking(): ThreadTracking {
    return \Drupal::service('webform_email_reply_threads.thread_tracking');
  }

}
